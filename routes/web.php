<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('auth.login');
});

// Route::get('/bootcamp', function () {
//     return view('bootcamp.index');
// });

// Route::get('/bootcamp', [UserController::class, 'index']);
// Route::get('/bio', [BioController::class, 'index']);


// Auth
Route::group(['middleware' => ['guest']], function () {
    Route::get('/login', 'AuthController@login');
    Route::post('/login', 'AuthController@show');
    Route::get('/register', 'AuthController@register');
    Route::post('/register', 'AuthController@create');
});

Route::group(['middleware' => ['session.login']], function () {
    Route::get('/dashboard', 'DashboardController@index');

    // Member
    Route::get('/member', 'UserController@index');
    Route::get('/member-detail/{id}', 'UserController@show');
    Route::get('/member-edit/{id}', 'UserController@edit');
    Route::post('/member-update', 'UserController@update');
    Route::get('/member-delete/{id}', 'UserController@delete');

    // Bio
    Route::get('/bio', 'BioController@index');

    // Logout
    Route::get('/logout', 'AuthController@logout');
});

// Route::resource('/menu-1', 'controller');