@extends('layout/main')
@section('page-name', 'Member')
@section('breadcrumbs', 'Member')
@section('menu-bootcamp', 'active')
@section('content')

    <!-- general form elements -->
    <div class="card {{ isset($detail) ? 'card-primary' : 'card-warning' }}">
        <div class="card-header">
            <h3 class="card-title">Data Peserta Bootcamp 
                @if(isset($detail['id']))
                    {{$detail['id']}}
                @else
                    {{$edit['id']}}
                @endif
            </h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if(isset($detail['id']))
            <form>
        @else
            <form action="{{ url('/member-update') }}" method="POST">
        @endif
            @csrf
            <div class="card-body">
                <input type="hidden" name="id" value="{{ isset($detail['id']) ? $detail['id'] : $edit['id'] }}">
                <div class="form-group">
                    <label for="name">Nama Lengkap</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama"
                        value="{{ isset($detail['name']) ? $detail['name'] : $edit['name'] }}" 
                        @unless(isset($edit)) 
                            readonly 
                        @endunless>
                </div>
                <div class="form-group">
                    <label for="univ">Asal Universitas</label>
                    <input type="text" class="form-control" id="univ" name="univ" placeholder="Masukkan Nama Universitas" 
                        value="{{ isset($detail['univ']) ? $detail['univ'] : $edit['univ'] }}"
                        @unless(isset($edit)) 
                            readonly 
                        @endunless>
                </div>
                <div class="form-group">
                    <label for="asal">Asal</label>
                    <input type="text" class="form-control" id="asal" name="asal" placeholder="Masukkan Asal"
                        value="{{ isset($detail['asal']) ? $detail['asal'] : $edit['asal'] }}"
                        @unless(isset($edit)) 
                            readonly 
                        @endunless>
                </div>
            </div>
            <!-- /.card-body -->
            @unless(isset($detail))
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            @endunless

        </form>
    </div>
    <!-- /.card -->

@endsection
