@extends('layout/main')
@section('page-name', 'Data Bootcamp')
@section('breadcrumbs', 'Data Bootcamp')
@section('menu-bootcamp', 'active')
@section('content')

    <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Bootcamp Berijalan Batch 9</h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right"
                                    placeholder="Search">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Lengkap</th>
                                    <th>Asal Kampus</th>
                                    <th>Asal Daerah</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row['name'] }}</td>
                                        {{-- <td>
                                            @if ($row['univ'] === 'Universitas Atma Jaya Yogyakarta')
                                                <span style="color: green; padding: 4px 6px; background-color: lightgreen; border-radius: 10px;">{{ $row['univ'] }}</span>
                                            @elseif ($row['univ'] === 'Universitas Kristen Duta Wacana')
                                                <span style="color: blue; padding: 4px 6px; background-color: lightblue; border-radius: 10px;">{{ $row['univ'] }}</span>
                                            @elseif ($row['univ'] === 'UPN Veteran Yogyakarta')
                                                <span style="color: red; padding: 4px 6px; background-color: pink; border-radius: 10px;">{{ $row['univ'] }}</span>
                                            @else
                                                <span style="color: gray;">{{ $row['univ'] }}</span>
                                            @endif
                                        </td> --}}
                                        <td>
                                            @if ($row['univ'] === 'Universitas Atma Jaya Yogyakarta')
                                                <span class="badge badge-success">{{ $row['univ'] }}</span>
                                            @elseif ($row['univ'] === 'Universitas Kristen Duta Wacana')
                                                <span class="badge badge-primary">{{ $row['univ'] }}</span>
                                            @elseif ($row['univ'] === 'UPN Veteran Yogyakarta')
                                                <span class="badge badge-danger">{{ $row['univ'] }}</span>
                                            @else
                                                <span class="badge badge-secondary">{{ $row['univ'] }}</span>
                                            @endif
                                        </td>
                                        <td>{{ $row['asal'] }}</td>
                                        <td>
                                            <a href="{{ url('/member-detail/'.$row->id) }}" class="btn btn-primary">Detail</a>
                                            <a href="{{ url('/member-edit/'.$row->id) }}" class="btn btn-warning">Edit</a>
                                            <a href="{{ url('/member-delete/'.$row->id) }}" class="btn btn-danger">Delete</a>
                                            {{-- <form action="{{ route('bootcamp.destroy', $row['id']) }}" method="POST" class="d-inline">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger"
                                                    onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')">Hapus</button>
                                            </form> --}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div><!-- /.container-fluid -->

@endsection
