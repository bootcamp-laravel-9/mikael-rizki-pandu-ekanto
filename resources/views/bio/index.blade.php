@extends('layout/main')
@section('page-name', 'Curriculum Vitae')
@section('breadcrumbs', 'Bio')
@section('menu-bio', 'active')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header" style="background-color: #4f87c0; color: white">
                        <h3 class="card-title">Personal Information</h3>
                    </div>
                    <div class="card-body">
                        <img src="{{ asset('AdminLTE') }}/dist/img/user1-128x128.jpg" alt="User Avatar"/>
                        <hr/>
                        <h3><strong>Mikael Rizki</strong></h3>
                        <h5>Bootcamp Berijalan Batch 9</h5>
                        <hr/>
                        <p><strong>Email:</strong> mikael.rizki.pe@gmail.com</p>
                        <p><strong>Phone:</strong> 087775972032</p>
                        <p><strong>Address:</strong> Jl. Wirajaya, Condongcatur, Depok, Sleman</p>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" style="background-color: #4f87c0; color: white">
                                <h3 class="card-title">Profile Summary</h3>
                            </div>
                            <div class="card-body">
                                <p>A highly skilled web developer with experience in building responsive websites using HTML, CSS,
                                    and JavaScript. Strong collaboration skills with designers to implement UI/UX designs and
                                    optimize websites for performance and search engines.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" style="background-color: #4980b6; color: white">
                                <h3 class="card-title">Education</h3>
                            </div>
                            <div class="card-body">
                                <h5>Bachelor of Science in Computer Science</h5>
                                <p>Duta Wacana Christian University, Yogyakarta, Indonesia (2020 - 2024)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" style="background-color: #4f87c0; color: white">
                                <h3 class="card-title">Work Experience</h3>
                            </div>
                            <div class="card-body">
                                <h5>Laravel Web Developer</h5>
                                <p>Berijalan (2024 - Present)</p>
                                <ul>
                                    <li>Developed responsive websites using HTML, CSS, and JavaScript</li>
                                    <li>Collaborated with designers to implement UI/UX designs</li>
                                    <li>Optimized websites for performance and search engines</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" style="background-color: #4f87c0; color: white">
                                <h3 class="card-title">Skills</h3>
                            </div>
                            <div class="card-body">
                                <ul>
                                    <li>HTML5</li>
                                    <li>CSS3</li>
                                    <li>JavaScript</li>
                                    <li>Responsive Design</li>
                                    <li>Version Control (Git)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" style="background-color: #4f87c0; color: white">
                                <h3 class="card-title">Certifications and Training</h3>
                            </div>
                            <div class="card-body">
                                <ul>
                                    <li>Web Development Certification - Online Course</li>
                                    <li>UX/UI Design Workshop - City College</li>
                                    <li>SEO Essentials Training - Online Course</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
        </div>
        <!-- /.row -->

    </div><!-- /.container-fluid -->

@endsection
