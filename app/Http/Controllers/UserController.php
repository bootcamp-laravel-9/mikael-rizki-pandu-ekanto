<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Contracts\View\View;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data =[
        //     ['nama' => 'Mikael Rizki', 'univ' => 'Universitas Kristen Duta Wacana', 'asal' => 'Yogyakarta'],
        //     ['nama' => 'Edwin Mahendra', 'univ' => 'Universitas Kristen Duta Wacana', 'asal' => 'Yogyakarta'],
        //     ['nama' => 'Imanuel Vicky', 'univ' => 'Universitas Kristen Duta Wacana', 'asal' => 'Tegal'],
        //     ['nama' => 'Adit Bagas', 'univ' => 'Universitas Atma Jaya Yogyakarta', 'asal' => 'Kalimantan Tengah'],
        //     ['nama' => 'Novianto', 'univ' => 'Universitas Atma Jaya Yogyakarta', 'asal' => 'Sumatera Selatan'],
        //     ['nama' => 'Rizal', 'univ' => 'UPN Veteran Yogyakarta', 'asal' => 'Sumedang'],
        //     ['nama' => 'Samuel', 'univ' => 'Universitas Atma Jaya Yogyakarta', 'asal' => 'Pontianak']
        // ];

        $data = Member::all();
        return view('member.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Member::find($id);
        // dd($detail);
        return view('member.item', compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Member::find($id);
        return view('member.item', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        // Validate the request
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'univ' => 'required',
            'asal' => 'required'
        ]);

        // dd($request->all());

        // Find the member by ID
        $updateMember = $member->find($request->id);

        // Update member fields
        $updateMember->id = $request->id;
        $updateMember->name = $request->name;
        $updateMember->univ = $request->univ;
        $updateMember->asal = $request->asal;

        // Redirect back or to another page after update
        if($updateMember->save()){
            return redirect('/member');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Member::destroy($id);
        return redirect('/member');
    }
}
