<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = [
            ['name' => 'Mikael Rizki', 'univ' => 'Universitas Kristen Duta Wacana', 'asal' => 'Yogyakarta'],
            ['name' => 'Edwin Mahendra', 'univ' => 'Universitas Kristen Duta Wacana', 'asal' => 'Yogyakarta'],
            ['name' => 'Imanuel Vicky', 'univ' => 'Universitas Kristen Duta Wacana', 'asal' => 'Tegal'],
            ['name' => 'Adit Bagas', 'univ' => 'Universitas Atma Jaya Yogyakarta', 'asal' => 'Kalimantan Tengah'],
            ['name' => 'Novianto', 'univ' => 'Universitas Atma Jaya Yogyakarta', 'asal' => 'Sumatera Selatan'],
            ['name' => 'Rizal', 'univ' => 'UPN Veteran Yogyakarta', 'asal' => 'Sumedang'],
            ['name' => 'Samuel', 'univ' => 'Universitas Atma Jaya Yogyakarta', 'asal' => 'Pontianak']
        ];

        DB::beginTransaction();

        foreach ($members as $member) {
            Member::firstOrCreate($member);
        }

        DB::commit();
    }
}
